## Gestion Biblioteca
### Introduccion

En este proyecto trato de enfocarme principalmente en la parte **backend** de la webapp. Se va a ir creando el proyecto y refactorizandolo para tratar de conseguir un codigo legible, mantenible y modular apoyandonos en **Patrones de Diseño** y en distintos **Frameworks**.

---
 Parte del proyecto sin terminar subido a **HEROKU** usando una base de datos **MySQL**, tener en cuenta que es todo FREE (la pagina tarda en la primer carga):

https://catalogo-peliculas.herokuapp.com/

---

### Frameworks utilizados hasta el momento: ###
+ Hibernate
+ JPA
+ Spring Framework

### Patrones de diseño Utilizados: ###
+ SRP
+ OCP
+ DRY
+ ISP
+ **MVC**
+ Patron Command
+ Patron DAO
+ PAtron Factory
+ Abstract Factory
+ **Inversion de Control**
+ **Inyeccion de Dependencia**

##### Otros #####
+ JDBC
+ Api Reflection
+ Manejo de excepciones y uso de Log4j
+ **TomCat**
+ **TDD**
+ **Maven**

##### Base de Datos #####
+ **MySql**


