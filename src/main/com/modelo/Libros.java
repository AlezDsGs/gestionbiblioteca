package main.com.modelo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.persistence.*;
import javax.transaction.Transaction;
import java.util.List;
import java.util.Objects;

import static java.lang.Integer.parseInt;

@Entity
@Table(name = "libreria")
public class Libros {
    @Id

    /*se usa la estrategia IDENTITY para que Hibernate use el AUTOINCREMENTO
    de la BBDD y  no intente crearlo él mismo*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "isbnbbdd")
    private int isbn;
    @Column(name = "titulobbdd")
    private String titulo;

    @ManyToOne
    @JoinColumn(name = "categoriabbdd")
    private Categoria categoria;

    public Libros(String titulo,Categoria categoria){
        this.titulo=titulo;
        this.categoria=categoria;
    }

    public Libros(){
    }



    public int getIsbn() {
        return isbn;
    }


    public void setIsbn(int isbn) {

        this.isbn = isbn;
    }

    public int getId(){
        return isbn;
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Libros libros = (Libros) o;
        return Objects.equals(titulo, libros.titulo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titulo);
    }

}
