package main.com.modelo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "categorizacion")
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcategoria")
    private int id;

    @Column(name = "descripcionbbdd")
    private String descripcion;

    @OneToMany(targetEntity = Libros.class, mappedBy = "categoria",
    cascade = CascadeType.ALL,orphanRemoval = true,
    fetch = FetchType.LAZY)
    private List<Libros> listaDeLibros;


    public Categoria() {
    }

    public Categoria(String descripcion) {
        this.descripcion=descripcion;
    }



    public List<Libros> getListaDeLibros() {
        return listaDeLibros;
    }

    public void setListaDeLibros(List<Libros> listaDeLibros) {
        this.listaDeLibros = listaDeLibros;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return Objects.equals(id, categoria.id) &&
                Objects.equals(descripcion, categoria.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, descripcion);
    }
}
