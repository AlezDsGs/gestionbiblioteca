package main.com.dao;

import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import java.util.List;

public interface LibrosDAO extends GenericoDAO<Libros,Integer>{

    public abstract List<Libros> buscadorPorCategoria(Categoria categoria);



}
