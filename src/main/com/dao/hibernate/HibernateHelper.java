package main.com.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateHelper {

    private static final SessionFactory factoria=crearFactoria();

    private static SessionFactory crearFactoria() {

        SessionFactory factoria=null;


        try {

            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure().build();

            Metadata metaData =
                    new MetadataSources(standardRegistry).getMetadataBuilder().build();
            factoria = metaData.getSessionFactoryBuilder().build();

        }catch (HibernateException e){
            System.out.println("EL ERROR ESTA EN: "+e.getMessage());
            System.out.println("un poco mas en---: "+e.getCause());
            System.out.println("un poco mas: "+e.getLocalizedMessage());
        }
        return factoria;
    }

    public static SessionFactory obtenerSessionFactory(){
        return factoria;
    }

}
