package main.com.dao.hibernate;

import main.com.dao.CategoriaDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.GenericoDAOJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import java.util.List;

public class CategoriaDAOImplHibernate extends GenericoDAOJPA<Categoria,Integer> implements CategoriaDAO {
    @Override
    public Categoria buscarIdPorDescripcion(String descripcion) {
        return null;
    }

    @Override
    public List<Libros> buscarPorCategoria(String categoria) {
        return null;
    }
}
