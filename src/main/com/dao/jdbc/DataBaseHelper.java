package main.com.dao.jdbc;

import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DataBaseHelper <T>{
    private static final Logger log= Logger.getLogger("deBBDD");


    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/biblioteca";
    private static final String USER = "root";
    private static final String PASS = "notebookbangho";

    public int modificarRegistro(String consultaSQL)
    {

        int filasAfectadas=0;
        Connection conexion =null;
        Statement sentencias =null;
       try {
            Class.forName(DRIVER);
            conexion = DriverManager.getConnection(URL, USER, PASS);
            sentencias = conexion.createStatement();
            filasAfectadas = sentencias.executeUpdate(consultaSQL);


       }catch (ClassNotFoundException e){
           log.error("error en CLASS: "+e.getMessage());
           throw new DataBaseException("Clase no encontrada",e);
       }catch (SQLException e){
           log.error("error en consulta SQL o cerrando conexiones "+e.getMessage());
           throw new DataBaseException("Error en consulta SQL",e);
       }



        return filasAfectadas;

    }

    public List<T> seleccionarRegistro(String consultaSQL, Class clase)
    {
        ResultSet rs = null;
        List<T> listaDeObjetos=new ArrayList<T>();
        Connection conexion=null;
        Statement sentencias=null;

        try {
            Class.forName(DRIVER);
             conexion = DriverManager.getConnection(URL, USER, PASS);
             sentencias = conexion.createStatement();
            rs = sentencias.executeQuery(consultaSQL);


            while (rs.next()) {
                T objeto= (T) (Class.forName(clase.getName())).newInstance();

                Method[] metodos=objeto.getClass().getDeclaredMethods();
                for (int i=0;i<metodos.length;i++) {
                    if (metodos[i].getName().startsWith("set") ) {
                        metodos[i].invoke(objeto,rs.getString(metodos[i].getName().substring(3)));
                    }
                    if (objeto.getClass().getName().equals("java.lang.String")) {
                        objeto=(T)rs.getString(1);
                    }
                }
                listaDeObjetos.add(objeto);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new DataBaseException("Error al instanciar la Clase especificada, metodo seleccionarRegistro",e);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new DataBaseException("Error al invocar, metodo seleccionarRegistro",e);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DataBaseException("error en la sentencia SQL, metodo seleccionarRegistro",e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new DataBaseException("No se pudo conceder el acceso, metodo seleccionarRegistro",e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new DataBaseException("la Clase no funciona, metodo seleccionarRegistro()",e);
        }

        return listaDeObjetos;
    }

}

