package main.com.dao;

import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import java.util.List;

public interface CategoriaDAO extends GenericoDAO <Categoria,Integer> {

    public abstract Categoria buscarIdPorDescripcion(String descripcion);
    public abstract List<Libros> buscarPorCategoria(String categoria);

}
