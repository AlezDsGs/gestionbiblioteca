package main.com.dao.jpa;

import main.com.dao.CategoriaDAO;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

public class CategoriaDAOimplJPA extends GenericoDAOJPA<Categoria,Integer> implements CategoriaDAO {


    public  Categoria buscarIdPorDescripcion(String descripcion){
        EntityManager em= getEntityManagerFactory().createEntityManager();

        TypedQuery<Categoria> consulta=em.createQuery("SELECT c from Categoria c  where c.descripcion=?1",Categoria.class);
        consulta.setParameter(1,descripcion);

        Categoria categ=consulta.getSingleResult();

        em.close();

        return categ;

    }

    public  List<Libros> buscarPorCategoria(String categoria){

        Integer idCategoria=Integer.parseInt(categoria);

        EntityManager em= getEntityManagerFactory().createEntityManager();

        TypedQuery consulta=em.createQuery("SELECT l from Libros l join fetch l.categoria where l.categoria.id=?1",Libros.class);

        consulta.setParameter(1,idCategoria);


        List<Libros> listaDeLibros=consulta.getResultList();

        return listaDeLibros;

    }
}
