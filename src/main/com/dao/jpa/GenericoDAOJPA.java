package main.com.dao.jpa;

import main.com.controlador.acciones.Accion;
import main.com.dao.GenericoDAO;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class GenericoDAOJPA<T,Id extends Serializable> implements GenericoDAO<T,Id>
{

    private EntityManagerFactory entityManagerFactory;



    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    private Class<T> claseDePersistencia;

    public GenericoDAOJPA() {
        this.claseDePersistencia = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }


    public List<T> buscarTodos()
    {
        List<T> listaDeObjetos=null;


        EntityManager em= getEntityManagerFactory().createEntityManager();

        TypedQuery<T> consulta=em.createQuery("SELECT o FROM "+ claseDePersistencia.getSimpleName()+" o ",claseDePersistencia);

        try {
            listaDeObjetos = consulta.getResultList();
        }finally {
            em.close();
        }

        return listaDeObjetos;


    }

    public T buscarClave(Id id)
    {

        EntityManager em= getEntityManagerFactory().createEntityManager();

        T objeto=(T)em.find(claseDePersistencia,id);

        em.close();

        return objeto;



    }



    public void insertar(T objeto)
    {
        EntityManager em= getEntityManagerFactory().createEntityManager();
        EntityTransaction transaccion=em.getTransaction();
        transaccion.begin();

        em.persist(objeto);
        transaccion.commit();
    }

    public void borrar(T objeto) {
        EntityManager em= getEntityManagerFactory().createEntityManager();

        try {

            Method metodo=claseDePersistencia.getMethod("getId");

            int id= (int) metodo.invoke(objeto);


            T obj=em.find(claseDePersistencia,id);

            EntityTransaction transaccion = em.getTransaction();
            transaccion.begin();
            em.remove(obj);
            transaccion.commit();

        }catch (PersistenceException e){
            em.getTransaction().rollback();

            throw e;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            System.out.println("no se encuentra el metodo especificado");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

    }

    public void guardarCambios(T objeto)
    {

        EntityManager em= getEntityManagerFactory().createEntityManager();

        EntityTransaction transaccion=em.getTransaction();
        transaccion.begin();

        em.merge(objeto);

        transaccion.commit();

        em.close();

    }





}
