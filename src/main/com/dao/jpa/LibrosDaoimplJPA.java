package main.com.dao.jpa;

import main.com.dao.LibrosDAO;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.persistence.*;
import java.util.List;

public class LibrosDaoimplJPA extends GenericoDAOJPA<Libros, Integer> implements LibrosDAO {



    public List<Libros> buscadorPorCategoria(Categoria categoria)
    {

        EntityManager em= getEntityManagerFactory().createEntityManager();



        TypedQuery<Libros> consulta=em.createQuery("SELECT l from Libros l where l.categoria=?1",Libros.class);

        consulta.setParameter(1,categoria);

        List<Libros> listaDeLibros=consulta.getResultList();

        return listaDeLibros;


    }


}
