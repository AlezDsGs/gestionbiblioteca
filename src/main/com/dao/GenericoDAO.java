package main.com.dao;

import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

public interface GenericoDAO <T,Id extends Serializable> {

    public  abstract List<T> buscarTodos();
    public abstract void insertar(T objeto);
    public abstract void borrar(T objeto);
    public abstract void guardarCambios(T objeto);
    public abstract T buscarClave(Id id);

}
