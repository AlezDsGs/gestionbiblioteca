package main.com.controlador;

import main.com.controlador.acciones.Accion;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ControladorLibros extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        RequestDispatcher despachador = null;
        Accion accion=null;
        String url=req.getPathInfo();

        accion=Accion.getAccion(url.substring(1,url.length()-3));
        despachador=req.getRequestDispatcher(accion.ejecutar(req,resp));
        despachador.forward(req,resp);
    }
}
