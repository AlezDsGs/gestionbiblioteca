package main.com.controlador.acciones;

import main.com.dao.LibrosDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BorrarLibroAccion extends Accion {
    private LibrosDAO liDAO=new LibrosDaoimplJPA();


    public String ejecutar(HttpServletRequest req, HttpServletResponse resp) {
        int isbn=Integer.parseInt(req.getParameter("isbn"));

        Libros libro1=new Libros();
        libro1.setIsbn(isbn);

        liDAO.borrar(libro1);


        return "MostrarLibros.do";
    }
}


