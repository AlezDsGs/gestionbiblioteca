package main.com.controlador.acciones;

import main.com.dao.CategoriaDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class FormularioInsertarLibroAccion extends Accion {

    private CategoriaDAOimplJPA caDAO=new CategoriaDAOimplJPA();

    @Override
    public String ejecutar(HttpServletRequest req, HttpServletResponse resp) {

        List<Categoria>listaDeCategorias=null;
        listaDeCategorias= caDAO.buscarTodos();
        req.setAttribute("listaDeCategorias",listaDeCategorias);
        return "/FormularioAgregarLibro.jsp";


    }
}


