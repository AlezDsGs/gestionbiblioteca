package main.com.controlador.acciones;

import main.com.dao.CategoriaDAO;
import main.com.dao.LibrosDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;
import main.com.servicios.ServicioBiblioteca;
import main.com.servicios.implementacion.ServicioBibliotecaImp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class MostrarLibrosAccion extends Accion {


    @Override
    public String ejecutar(HttpServletRequest req, HttpServletResponse resp) {

        ServicioBiblioteca servicio= (ServicioBiblioteca) obtenerBean("ServicioBiblioteca",req);

        /* AHORA ESTA INYECCION DE DEPENDENCIA A TRAVES DE GETTERS Y SETTERS
        SE HACE DESDE EL FICHERO contextoDeAplicacion.xml Y POR ESO YA NO
        NECESITAMOS ESTAS LINEAS:

        LibrosDAO librosDAO= (LibrosDAO) obtenerBean("LibrosDAO");

        CategoriaDAO categoriaDAO= (CategoriaDAO) obtenerBean("CategoriaDAO");

        servicio.setLibrosDAO(librosDAO);
        servicio.setCategoriaDAO(categoriaDAO);

         */



        List<Libros>listaDeLibros=null;
        List<Categoria>listaDeCategorias= servicio.BuscarCategorias();


        String categoria=req.getParameter("categoria");


        if (categoria == null || categoria.equals("seleccionar")) {
            listaDeLibros = servicio.buscarTodosLosLibros();
        } else {
            Categoria categ=new Categoria();
            categ.setId(Integer.parseInt(categoria));
            listaDeLibros = servicio.buscarLibrosPorCategoria(categ);
        }

        req.setAttribute("listaDeLibros", listaDeLibros);
        req.setAttribute("listaDeCategorias", listaDeCategorias);

        return "/MostrarLibros.jsp";

    }


}


