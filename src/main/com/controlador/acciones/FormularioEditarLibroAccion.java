package main.com.controlador.acciones;

import main.com.dao.CategoriaDAO;
import main.com.dao.LibrosDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class FormularioEditarLibroAccion extends Accion {
    private LibrosDaoimplJPA liDAO=new LibrosDaoimplJPA();
    private CategoriaDAOimplJPA caDAO=new CategoriaDAOimplJPA();

    @Override
    public String ejecutar(HttpServletRequest req, HttpServletResponse resp) {

        List<Categoria>listaDeCategorias=null;

        int isbn=Integer.parseInt(req.getParameter("isbn"));
        listaDeCategorias= caDAO.buscarTodos();
        Libros libro= liDAO.buscarClave(isbn);

        req.setAttribute("listaDeCategorias",listaDeCategorias);
        req.setAttribute("libro",libro);

        return "/FormularioEditarLibro.jsp";

    }
}


