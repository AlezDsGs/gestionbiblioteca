package main.com.controlador.acciones;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceContext;


public abstract class Accion {


    public abstract String ejecutar(HttpServletRequest req, HttpServletResponse resp);

    public static Accion getAccion(String tipo){
        Accion accion=null;

        try{
            accion = (Accion) Class.forName("main.com.controlador.acciones."+tipo+"Accion").newInstance();


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return accion;
    }


    public Object obtenerBean(String nombre, HttpServletRequest req){
        WebApplicationContext factoria=
                WebApplicationContextUtils
                        .getRequiredWebApplicationContext(req.getSession()
                                .getServletContext());


        return factoria.getBean(nombre);
    }

}
