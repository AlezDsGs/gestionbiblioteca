package main.com.controlador.acciones;

import main.com.dao.LibrosDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ModificarLibroAccion extends Accion {

    private LibrosDaoimplJPA liDAO=new LibrosDaoimplJPA();
    private CategoriaDAOimplJPA caDAO=new CategoriaDAOimplJPA();

    @Override
    public String ejecutar(HttpServletRequest req, HttpServletResponse resp) {

        String isbn=req.getParameter("isbn");
        String titulo=req.getParameter("titulo");
        String categoria=req.getParameter("categoria");

        Categoria categoriaNueva=new Categoria();
        categoriaNueva.setId(Integer.parseInt(categoria));

        Libros libro=new Libros(titulo,categoriaNueva);
        libro.setIsbn(Integer.parseInt(isbn));


        liDAO.guardarCambios(libro);

        return"MostrarLibros.do?categoria=seleccionar";
    }
}


