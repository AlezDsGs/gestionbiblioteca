package main.com.controlador.acciones;

import main.com.dao.CategoriaDAO;
import main.com.dao.LibrosDAO;
import main.com.dao.jpa.CategoriaDAOimplJPA;
import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class InsertarLibroAccion extends Accion {
    private LibrosDaoimplJPA liDAO=new LibrosDaoimplJPA();
    private CategoriaDAOimplJPA caDAO=new CategoriaDAOimplJPA();


    public String ejecutar(HttpServletRequest req, HttpServletResponse resp) {

        String titulo=req.getParameter("titulo");
        int idCategoria=Integer.parseInt(req.getParameter("categoria"));

        Categoria categoria= caDAO.buscarClave(idCategoria);

        Libros libro=new Libros(titulo,categoria);

        liDAO.insertar(libro);

        return "MostrarLibros.do?categoria=seleccionar";

    }
}


