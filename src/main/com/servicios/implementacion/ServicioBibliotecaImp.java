package main.com.servicios.implementacion;

import main.com.dao.CategoriaDAO;
import main.com.dao.LibrosDAO;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;
import main.com.servicios.ServicioBiblioteca;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class ServicioBibliotecaImp implements ServicioBiblioteca {

    private LibrosDAO librosDAO=null;
    private CategoriaDAO categoriaDAO=null;



    public ServicioBibliotecaImp() {

    }


    @Override
    public void guardarCambios(Libros libro) {
        librosDAO.guardarCambios(libro);

    }

    @Override
    public void borrarLibro(Libros libro) {
        librosDAO.borrar(libro);

    }

    @Override
    public List<Libros> buscarTodosLosLibros() {
        return librosDAO.buscarTodos();
    }

    @Override
    public List<Categoria> BuscarCategorias() {
        return categoriaDAO.buscarTodos();
    }

    @Override
    public Libros BuscarLibroPorClave(int isbn) {
        return librosDAO.buscarClave(isbn);
    }

    @Override
    public Categoria BuscarCategoriaPorClave(int id) {
        return categoriaDAO.buscarClave(id);
    }

    @Override
    public List<Libros> buscarLibrosPorCategoria(Categoria categoria) {
        return librosDAO.buscadorPorCategoria(categoria);
    }

    @Override
    public void setLibrosDAO(LibrosDAO librosDAO) {
        this.librosDAO=librosDAO;
    }

    @Override
    public LibrosDAO getLibrosDAO() {
        return librosDAO;
    }

    @Override
    public void setCategoriaDAO(CategoriaDAO categoriaDAO) {
        this.categoriaDAO=categoriaDAO;

    }

    @Override
    public CategoriaDAO getCategoriaDAO() {
        return categoriaDAO;
    }
}
