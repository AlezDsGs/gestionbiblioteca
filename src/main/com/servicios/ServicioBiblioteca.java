package main.com.servicios;

import main.com.dao.CategoriaDAO;
import main.com.dao.LibrosDAO;
import main.com.modelo.Categoria;
import main.com.modelo.Libros;

import java.util.List;

public interface ServicioBiblioteca {
    public void guardarCambios(Libros libro);

    public void borrarLibro(Libros libro);
    public List<Libros> buscarTodosLosLibros();

    public List<Categoria> BuscarCategorias();
    public Libros BuscarLibroPorClave(int isbn);

    public Categoria BuscarCategoriaPorClave(int id);
    public List<Libros> buscarLibrosPorCategoria(Categoria categoria);


    public void setLibrosDAO(LibrosDAO librosDAO);
    public LibrosDAO getLibrosDAO();
    public void setCategoriaDAO(CategoriaDAO categoriaDAO);
    public CategoriaDAO getCategoriaDAO();

}
