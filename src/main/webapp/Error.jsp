<%@page isErrorPage="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Manejo de Errores</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/recursos/estilo.css" type="text/css">

</head>
<body>

Ha ocurrido un error en la aplicacion : <%=exception.getMessage()%>
<br/>
<br/>
Error interno : <%=exception.getCause().getMessage()%>

</body>
</html>
