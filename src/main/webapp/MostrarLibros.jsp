<%@ page import="main.com.modelo.Libros" %>
<%@ page import="java.util.List" %>
<%@ page import="main.com.dao.jdbc.DataBaseException" %>
<%@ page import="main.com.modelo.Categoria" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>Mostrar Libros</title>
    <meta charset="UTF-8">
    <link rel="stylesheet"  href="<%=request.getContextPath()%>/recursos/estilo.css" type="text/css">
    <script type="text/javascript" src="<%=request.getContextPath()%>/recursos/interaccion.js"></script>
</head>
<body>

<header>
    <h1 id="titulo">Gestion Libros</h1>
</header>



<form id="categorias" action="<%=request.getContextPath()%>/ControladorLibros/MostrarLibros.do">
    <select name="categoria">

        <option value="seleccionar">Seleccionar</option>

        <% List<Categoria> listaDeCategorias=null;
            listaDeCategorias=(List<Categoria>)request.getAttribute("listaDeCategorias");

            for(Categoria categoria:listaDeCategorias){
                if (categoria.getDescripcion().equals(request.getParameter("categoria"))){
        %>
        <option value="<%=categoria.getId()%>" selected ><%=categoria.getDescripcion()%></option>
        <% } else { %>
        <option value="<%=categoria.getId()%>"><%=categoria.getDescripcion()%></option>
        <%}
        }
        %>

    </select><br/>

    <p><input type="submit" value="filtrar"/></p>
</form>


<% List<Libros> listaDeLibros=null;

    listaDeLibros= (List<Libros>) request.getAttribute("listaDeLibros");
%>
<%
    if(listaDeLibros.size()==0) {%>

<h1 id="noexiste">No existen Libros en esta Categoria</h1>
<%
    }else {%>

<div id="agregarlibro">
    <a href="<%=request.getContextPath()%>/ControladorLibros/FormularioInsertarLibro.do?isbn=<%=listaDeLibros.get(listaDeLibros.size()-1).getIsbn()%>" id="insertarlibro">
        Insertar Libro</a>
</div>


<%
    }
%>




<section id="galeria">



<%

    for(Libros libro:listaDeLibros){

%>

    <article>
    <%=libro.getTitulo()+" "%>
    </br>
        <%=libro.getCategoria().getDescripcion()+" "%>

        <img src="<%=request.getContextPath()%>/recursos/img/<%=libro.getIsbn()%>.jpg" /> <figcaption></figcaption>

        <a href="FormularioEditarLibro.do?isbn=<%=libro.getIsbn()%>" id="editarlibro">editar</a>
        <a href="BorrarLibro.do?isbn=<%=libro.getIsbn()%>" id="borrarlibro">borrar</a>
<br/>
    </article>
<%
    }%>




</section>

</body>
</html>