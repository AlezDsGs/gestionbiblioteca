<%@ page import="java.io.IOException" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.List" %>
<%@ page import="main.com.modelo.Libros" %>
<%@ page import="main.com.dao.jdbc.DataBaseException" %>
<%@ page import="main.com.modelo.Categoria" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<%=request.getContextPath()%>recursos/estilo.css" type="text/css">

    <!--<script type="text/javascript" src="interaccion.js"></script>-->
    <title>Formulario Libro</title>
</head>

<body>

<form action="<%=request.getContextPath()%>/ControladorLibros/InsertarLibro.do" onsubmit="return validacion();">
    <fieldset>
        <legend>Formulario agregar libro</legend>

        <p><label>ISBN : <%=Integer.parseInt(request.getParameter("isbn"))+1%></label></p>

        <p><label for="titulo">Titulo</label> <input id="titulo" type="text" name="titulo"></p>

<p  id="categoria"><select name="categoria">
    <option value="seleccionar">Seleccionar</option>

<%

        List<Categoria> rs=null;
        rs=(List<Categoria>) request.getAttribute("listaDeCategorias");
        for (Categoria categoria1:rs){ %>
<option value="<%=categoria1.getId()%>">
    <%=categoria1.getDescripcion()%></option>
<% }
%>
    <input type="submit" value="insertar"/>
    </fieldset>
</select>
    </p>
</form>
</body>
</html>