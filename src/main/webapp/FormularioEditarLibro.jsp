<%@ page import="main.com.modelo.Libros" %>
<%@ page import="java.util.List" %>
<%@ page import="main.com.dao.jdbc.DataBaseException" %>
<%@ page import="main.com.modelo.Categoria" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Formulario editar Libro</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/recursos/estilo.css" type="text/css">

</head>
<body>

<form id="editarlibro" action="<%=request.getContextPath()%>/ControladorLibros/ModificarLibro.do">
    <fieldset>
        <legend>formulario editar libro</legend>
        <p><label for="isbn"> ISBN: </label><input type="hidden" id="isbn" name="isbn" value="<%=((Libros)request.getAttribute("libro")).getIsbn()%>"> <%=((Libros)request.getAttribute("libro")).getIsbn()%></p>
        <p><label for="titulo">TITULO: </label><input type="text" id="titulo" name="titulo" value="<%=((Libros)request.getAttribute("libro")).getTitulo()%>"></p>

        <p><label for="categoria">CATEGORIA: </label>
        <select name="categoria" id="categoria">
            <option value="seleccionar">seleccionar</option>
            <%
                List<Categoria>listaDeCategorias=null;
                listaDeCategorias=(List<Categoria>)request.getAttribute("listaDeCategorias");
                Libros unLibro= (Libros) request.getAttribute("libro");
                Categoria categoDelLibro=unLibro.getCategoria();

                for (Categoria catego:listaDeCategorias){
                    if(catego.getDescripcion().equals(categoDelLibro.getDescripcion())){%>
            <option value="<%=catego.getId()%>" selected="selected"><%=catego.getDescripcion()%></option>
            <% }else{ %>
            <option value="<%=catego.getId()%>"><%=catego.getDescripcion()%></option>
            <%}
            }
         %>

        </select>
            <br/>
        </p>
        <p><input type="submit" value="salvar"/></p>
    </fieldset>
</form>
</body>
</html>
