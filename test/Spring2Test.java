import main.com.dao.jpa.LibrosDaoimplJPA;
import main.com.modelo.Libros;
import main.com.servicios.ServicioBiblioteca;
import main.com.servicios.implementacion.ServicioBibliotecaImp;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import javax.persistence.TypedQuery;
import java.util.List;

public class Spring2Test {
    @Test
    public void hola() {

        ClassPathXmlApplicationContext factoria = new
                ClassPathXmlApplicationContext("META-INF/contextoDeAplicacion.xml");

        ServicioBiblioteca serv= (ServicioBiblioteca) factoria.getBean("ServicioBiblioteca");

       List<Libros> lista=serv.buscarTodosLosLibros();

       for (Libros li:lista){
           System.out.println(li.getTitulo());
       }

    }
}
